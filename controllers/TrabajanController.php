<?php

namespace app\controllers;

use Yii;
use app\models\Trabajan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrabajanController implements the CRUD actions for Trabajan model.
 */
class TrabajanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trabajan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Trabajan::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Trabajan model.
     * @param integer $Id
     * @param integer $Cod_Grupo
     * @param integer $DNI_Empleado
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($Id, $Cod_Grupo, $DNI_Empleado)
    {
        return $this->render('view', [
            'model' => $this->findModel($Id, $Cod_Grupo, $DNI_Empleado),
        ]);
    }

    /**
     * Creates a new Trabajan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Trabajan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Id' => $model->Id, 'Cod_Grupo' => $model->Cod_Grupo, 'DNI_Empleado' => $model->DNI_Empleado]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Trabajan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $Id
     * @param integer $Cod_Grupo
     * @param integer $DNI_Empleado
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($Id, $Cod_Grupo, $DNI_Empleado)
    {
        $model = $this->findModel($Id, $Cod_Grupo, $DNI_Empleado);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Id' => $model->Id, 'Cod_Grupo' => $model->Cod_Grupo, 'DNI_Empleado' => $model->DNI_Empleado]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Trabajan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $Id
     * @param integer $Cod_Grupo
     * @param integer $DNI_Empleado
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($Id, $Cod_Grupo, $DNI_Empleado)
    {
        $this->findModel($Id, $Cod_Grupo, $DNI_Empleado)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Trabajan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $Id
     * @param integer $Cod_Grupo
     * @param integer $DNI_Empleado
     * @return Trabajan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Id, $Cod_Grupo, $DNI_Empleado)
    {
        if (($model = Trabajan::findOne(['Id' => $Id, 'Cod_Grupo' => $Cod_Grupo, 'DNI_Empleado' => $DNI_Empleado])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
