<?php

namespace app\controllers;

use Yii;
use app\models\Dirigen;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DirigenController implements the CRUD actions for Dirigen model.
 */
class DirigenController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dirigen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Dirigen::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Dirigen model.
     * @param integer $Tarjeta_Social
     * @param integer $DNI_Empleado
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($Tarjeta_Social, $DNI_Empleado)
    {
        return $this->render('view', [
            'model' => $this->findModel($Tarjeta_Social, $DNI_Empleado),
        ]);
    }

    /**
     * Creates a new Dirigen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dirigen();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tarjeta_Social' => $model->Tarjeta_Social, 'DNI_Empleado' => $model->DNI_Empleado]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Dirigen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $Tarjeta_Social
     * @param integer $DNI_Empleado
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($Tarjeta_Social, $DNI_Empleado)
    {
        $model = $this->findModel($Tarjeta_Social, $DNI_Empleado);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Tarjeta_Social' => $model->Tarjeta_Social, 'DNI_Empleado' => $model->DNI_Empleado]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Dirigen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $Tarjeta_Social
     * @param integer $DNI_Empleado
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($Tarjeta_Social, $DNI_Empleado)
    {
        $this->findModel($Tarjeta_Social, $DNI_Empleado)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Dirigen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $Tarjeta_Social
     * @param integer $DNI_Empleado
     * @return Dirigen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Tarjeta_Social, $DNI_Empleado)
    {
        if (($model = Dirigen::findOne(['Tarjeta_Social' => $Tarjeta_Social, 'DNI_Empleado' => $DNI_Empleado])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
