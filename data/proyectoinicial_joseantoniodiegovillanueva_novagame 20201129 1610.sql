﻿
CREATE DATABASE proyectoinicial_joseantoniodiegovillanueva_novagame;


-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 29/11/2020 16:10:53
-- Server version: 5.5.5-10.1.40-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE proyectoinicial_joseantoniodiegovillanueva_novagame;

--
-- Drop table `becarios`
--
DROP TABLE IF EXISTS becarios;

--
-- Drop table `dirigen`
--
DROP TABLE IF EXISTS dirigen;

--
-- Drop table `trabajan`
--
DROP TABLE IF EXISTS trabajan;

--
-- Drop table `empleados`
--
DROP TABLE IF EXISTS empleados;

--
-- Drop table `administradores`
--
DROP TABLE IF EXISTS administradores;

--
-- Drop table `grupos`
--
DROP TABLE IF EXISTS grupos;

--
-- Drop table `proyectos`
--
DROP TABLE IF EXISTS proyectos;

--
-- Set default database
--
USE proyectoinicial_joseantoniodiegovillanueva_novagame;

--
-- Create table `proyectos`
--
CREATE TABLE proyectos (
  Id int(25) NOT NULL,
  Fecha date NOT NULL,
  Revision int(25) NOT NULL,
  PRIMARY KEY (Id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 546,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `grupos`
--
CREATE TABLE grupos (
  Nombre_Grupo varchar(40) NOT NULL,
  Integrantes int(25) NOT NULL,
  Cod_Grupo int(25) NOT NULL,
  PRIMARY KEY (Cod_Grupo)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 546,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `administradores`
--
CREATE TABLE administradores (
  Nombre varchar(40) NOT NULL,
  Apellidos varchar(40) NOT NULL,
  Puesto varchar(100) NOT NULL,
  Tarjeta_Social int(20) NOT NULL,
  PRIMARY KEY (Tarjeta_Social)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 546,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `empleados`
--
CREATE TABLE empleados (
  Nombre varchar(40) NOT NULL,
  Apellidos varchar(40) NOT NULL,
  DNI int(8) NOT NULL,
  PRIMARY KEY (DNI)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 546,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `trabajan`
--
CREATE TABLE trabajan (
  Id int(25) NOT NULL,
  Cod_Grupo int(25) NOT NULL,
  DNI_Empleado int(8) NOT NULL,
  PRIMARY KEY (Id, Cod_Grupo, DNI_Empleado)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 546,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `Id` on table `trabajan`
--
ALTER TABLE trabajan
ADD UNIQUE INDEX Id (Id, DNI_Empleado);

--
-- Create foreign key
--
ALTER TABLE trabajan
ADD CONSTRAINT trabajan_ibfk_1 FOREIGN KEY (Id)
REFERENCES proyectos (Id);

--
-- Create foreign key
--
ALTER TABLE trabajan
ADD CONSTRAINT trabajan_ibfk_2 FOREIGN KEY (Cod_Grupo)
REFERENCES grupos (Cod_Grupo);

--
-- Create foreign key
--
ALTER TABLE trabajan
ADD CONSTRAINT trabajan_ibfk_3 FOREIGN KEY (DNI_Empleado)
REFERENCES empleados (DNI);

--
-- Create table `dirigen`
--
CREATE TABLE dirigen (
  Tarjeta_Social int(25) NOT NULL,
  DNI_Empleado int(25) NOT NULL,
  PRIMARY KEY (Tarjeta_Social, DNI_Empleado)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 546,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE dirigen
ADD CONSTRAINT dirigen_ibfk_1 FOREIGN KEY (Tarjeta_Social)
REFERENCES administradores (Tarjeta_Social);

--
-- Create foreign key
--
ALTER TABLE dirigen
ADD CONSTRAINT dirigen_ibfk_2 FOREIGN KEY (DNI_Empleado)
REFERENCES empleados (DNI);

--
-- Create table `becarios`
--
CREATE TABLE becarios (
  Nombre varchar(40) NOT NULL,
  Horas_Trabajadas int(25) NOT NULL,
  Tutor varchar(40) NOT NULL,
  DNI_Tutor int(25) NOT NULL,
  PRIMARY KEY (Tutor)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 546,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE becarios
ADD CONSTRAINT becarios_ibfk_1 FOREIGN KEY (DNI_Tutor)
REFERENCES empleados (DNI);

-- 
-- Dumping data for table proyectos
--
INSERT INTO proyectos VALUES
(1, '1993-05-15', -599382977),
(2, '1971-03-26', -1558087107),
(3, '2004-10-09', -111916084),
(4, '1979-05-24', -89948970),
(5, '2018-07-02', -1463700094),
(6, '1983-03-27', -782787959),
(7, '2005-09-08', -559690754),
(8, '1970-07-17', -1),
(9, '2001-03-03', -509434008),
(10, '1970-10-30', -94387014),
(11, '1972-06-18', -1476611773),
(12, '1982-10-17', -1677741864),
(13, '2016-11-01', -444154686),
(14, '1982-04-10', -280694610),
(15, '2005-08-14', -1135043815),
(16, '2006-07-15', -1029320644),
(17, '2004-03-01', -325571398),
(18, '1995-05-08', -1662410686),
(19, '1992-07-27', -1448783861),
(20, '1981-07-19', -421),
(21, '2019-05-22', -1562317690),
(22, '1970-01-10', 9),
(23, '1996-06-27', -1111872394),
(24, '1994-10-04', -1864233058),
(25, '1980-04-19', -436477743),
(26, '1989-12-23', -7296),
(27, '1971-02-04', -1937146433),
(28, '1978-03-12', -104211460),
(29, '2001-06-30', -1088592761),
(30, '1970-05-21', -140);

-- 
-- Dumping data for table grupos
--
INSERT INTO grupos VALUES
('1Z', -1568941220, 109916854),
('Z0', 32, 109916855),
('22N90HL', -1495667879, 109916856),
('FK8X093O5A829B073', -377981638, 324665220),
('N7T954', -5621, 324665221),
('619', -607968000, 324665222),
('T3', -9544, 489122339),
('UIP7G72WBT7', -652725614, 489122340),
('9U3B3F1RQ', -911948206, 703870704),
('E0M8', -811977915, 703870705),
('L629L3W63C26G8ZY369', -248229729, 911240417),
('7YLLUL7S11W70SQ2H63F493ZGU1P', -1063919768, 911240418),
('63OFY0I', 9, 911240419),
('7FKB6C5F0BH77H4A9G', -1059516407, 1125988783),
('05YV', 376, 1125988784),
('JI', -897662328, 1335816938),
('3', 0, 1335816939),
('4418Y8837Q5DZS', -1701107507, 1335816940),
('6HJGZ539A01J', -892955930, 1335816941),
('75769R', -1349122370, 1335816942),
('VZ6H71UHWAW9C5BW', -600167950, 1550565304),
('8LYA9F7', 52, 1550565305),
('3MI', 32, 1550565306),
('DEW8', -1497415048, 1550565307),
('0FE1292780WI72CG85PC', -1359489180, 1550565308),
('3', -2111890643, 1847341235),
('PRJJT33JS9CR440N8WZTL4', 515, 1847341236),
('916N679N42QI9', -1792038578, 2062089596),
('9J', -1549389145, 2062089597),
('P', 8197, 2147483647);

-- 
-- Dumping data for table administradores
--
INSERT INTO administradores VALUES
('I', '24936', 'A9W2V1', 432),
('0', '80460', 'WCC04WA7S70', 548150612),
('575I', '44963', '7192931C5', 548150613),
('E9DL2Q507C02', '34115', '942NUFQ6TV8QBV8D', 548150614),
('RHCC5M970', '69532', 'NXVP6UL3M2L2CE68XAU3V20D14NWAPL04NYT69A6008VA8N54161HNA0WS7Q9UI25M73LS9PY75I922893D25Z', 548150615),
('JQO10XA', '76457', 'AXOXK276386A8QFR84AX9VZ977W7P3MJF68W4CGXAQ5KUB5GU2G0MM4OAUQI06ISX316J6DST11Y7', 548150616),
('8KW', '08276', '41U61102E0EV1F47A9', 762898978),
('527T', '15878', '6ZADG818DQ16QY1OU1GE6YL0K5B08C75Z7903Q1P3RMQK', 762898979),
('8W7', '05398', 'ZY88098W937ISU648FJ9AGBQ7GY16Y4', 762898980),
('G3P', '93261', '4M46S3B0P0ZB1RJ8X545D5K8HVZ764159Q1SU2117L42R0WF736HUY36OKBNTNBR0DTY2BU14OD317TT5WHV8M158VLMMMR9CMQ4', 762898981),
('85', '38423', 'Q4T', 762898982),
('T3NO40JEX3LQ8SI15W4', '47864', '86E4J63SEJZC5016J29XU287N8G8H7379871K9B', 868816773),
('SVDCUE7230QKCZD65Y4N545K4X', '16874', '812N66M598NQJG099D79P747Y00T217LY27CG10112I5741', 868816774),
('4223', '96465', 'N9UC8EGJY0KS6020NS9433EMA5UB0BN8D4573A', 868816775),
('0', '19222', 'J6O39DFL85', 1083565138),
('62', '89169', '5XVJ114IA2IO7R8BQ6E1GOK83DC', 1083565139),
('CR71T', '51627', '2GTLZ', 1083565140),
('R7', '40093', '1N721N62J', 1303563038),
('J34HN4', '32014', '1K98', 1303563039),
('0R7Y67', '01414', 'A3T64', 1303563040),
('5W4CL', '76086', 'KBOCNBSC62352F9DLC97131EY1ZL9QR6322', 1303563041),
('Q74', '68767', '9M51', 1518311403),
('74Y7E', '37957', '2452DC52995CJU98056Q810VMJ', 1518311404),
('N3U', '88231', '46QR97JS4GP37RZ7K2L85', 1518311405),
('J6W', '55629', '8VP26XB046O7645NUV53LFNZ952E622N1WXI7GM39AMP50M', 1518311406),
('U', '23003', '5B5BNIL10U', 1908000232),
('4X306P44GU1', '33589', 'VRR35IZ0FQ9K62', 1908000233),
('D80X', '75351', 'R011HR8810360F607A', 2122748594),
('6RNORE', '82218', '7313WR1OKMJ45Q6KE3KZ4D', 2122748595),
('A84', '86849', 'PU77823H9ANM99OGB57T081J0', 2147483647);

-- 
-- Dumping data for table empleados
--
INSERT INTO empleados VALUES
('ZA9K25FD2356R3IKV7PC6AW', '60676', 32545513),
('75R1S9KU960HM', '65934', 32545514),
('5S26J20L3W92590LF2G6', '92568', 247293879),
('Z', '11427', 602646811),
('TZ768', '75243', 602646812),
('2MV', '91427', 602646813),
('811N9VJ', '74078', 602646814),
('K18TQ71R12', '96821', 817395176),
('P94', '27438', 817395177),
('0V', '48685', 817395178),
('B167A', '11796', 817395179),
('J', '98918', 1026851853),
('79DT3T6IDJ', '48708', 1026851854),
('T2345UEL', '91853', 1026851855),
('7Y9', '12831', 1241600218),
('8170', '67589', 1241600219),
('VR6K5V', '78096', 1241600220),
('VKHR0O019TRJ8', '09929', 1241600221),
('4J', '25316', 1363127590),
('3S4PV', '22638', 1363127591),
('6A0C', '91981', 1577875955),
('ITG9172L', '95191', 1577875956),
('0212L8S2ZNTNLK68WQ5', '49252', 1577875957),
('90A1UZJ72', '51365', 1915744137),
('4E9H52', '73979', 1915744138),
('320D2B3', '23539', 1915744139),
('J1Y5', '15938', 2130492498),
('5', '52663', 2130492499),
('8', '49673', 2130492500),
('ED6H', '27381', 2147483647);

-- 
-- Dumping data for table trabajan
--
INSERT INTO trabajan VALUES
(1, 109916854, 817395176),
(2, 2147483647, 1241600218),
(3, 703870704, 1577875955),
(4, 1335816938, 2147483647),
(5, 324665220, 2130492498),
(6, 911240417, 602646811),
(7, 1550565304, 817395177),
(8, 109916855, 1026851853),
(9, 489122339, 1241600219),
(10, 1335816939, 32545513),
(11, 2062089596, 1363127590),
(12, 1550565305, 602646812),
(13, 703870705, 817395178),
(14, 1335816940, 1026851854),
(15, 1125988783, 1241600220),
(16, 1550565306, 1577875956),
(17, 1335816941, 1915744137),
(18, 1550565307, 2130492499),
(19, 324665221, 1915744138),
(20, 911240418, 2130492500),
(21, 1125988784, 1363127591),
(22, 1847341235, 602646813),
(23, 109916856, 817395179),
(24, 2062089597, 1577875957),
(25, 489122340, 602646814),
(26, 911240419, 1026851855),
(27, 1335816942, 247293879),
(28, 1847341236, 32545514),
(29, 324665222, 1915744139),
(30, 1550565308, 1241600221);

-- 
-- Dumping data for table dirigen
--
INSERT INTO dirigen VALUES
(432, 1241600221),
(548150612, 817395176),
(548150613, 1026851853),
(548150614, 602646812),
(548150615, 1577875956),
(548150616, 2130492499),
(762898978, 2147483647),
(762898979, 1241600219),
(762898980, 1241600220),
(762898981, 1915744137),
(762898982, 1026851855),
(868816773, 817395177),
(868816774, 602646813),
(868816775, 1915744139),
(1083565138, 1241600218),
(1083565139, 817395178),
(1083565140, 1577875957),
(1303563038, 32545513),
(1303563039, 2130492500),
(1303563040, 817395179),
(1303563041, 32545514),
(1518311403, 602646811),
(1518311404, 1026851854),
(1518311405, 1363127591),
(1518311406, 247293879),
(1908000232, 2130492498),
(1908000233, 1915744138),
(2122748594, 1363127590),
(2122748595, 602646814),
(2147483647, 1577875955);

-- 
-- Dumping data for table becarios
--
INSERT INTO becarios VALUES
('3P3BR3', -12972683, '0499C3L1XC', 32545514),
('9YZ2', -429275820, '0846YQV498J6', 1915744138),
('706O1H', -409185753, '1', 1241600221),
('D0', -114995481, '1870', 1915744137),
('4H', -1231576311, '3', 1577875955),
('O', -307310126, '3Q6', 2147483647),
('1P', 6, '43A80W2K742SJ', 1026851853),
('M3S3', -8, '7PA33Z2C1K7Y0EJ7', 247293879),
('0MN8A5', -1209708887, '83', 1577875955),
('PE', -1448960231, '92V', 1915744137),
('44', -3631, '9D2V', 2130492499),
('2K0J', -556477826, '9F0', 32545514),
('95ACBY2MU73', -638715405, 'B', 817395179),
('A', -2133206387, 'B720206WQ1', 1026851855),
('4I', -622724199, 'D3W', 602646811),
('BM', -1624469049, 'DT3589R8A', 2147483647),
('HH7X23XN9', -532059750, 'DTB', 247293879),
('CZ89', 110, 'E56S94S7QQ7S00', 602646812),
('VS83M9', -476, 'E7UKXI02', 1363127590),
('XB7', -950747444, 'G15W8I1', 1577875957),
('4F9A', 63, 'G2', 817395178),
('WX2', -943839737, 'JK', 1026851854),
('171', -1490537754, 'K9W61M31M', 1241600221),
('SS78EHT841YU1047J', -207740402, 'PF', 1363127590),
('YE4TU831G', -546337011, 'RPBC50SNUKX6', 602646812),
('Y', -1922734024, 'UH', 1241600218),
('T6621N637K081394X51O050SS0S7', -1866654781, 'X', 1915744139),
('G8', -1655169076, 'XZ844H5357Z68670F2BB19ZP', 1026851855),
('SNV', -1618757887, 'YC568N45PIGRU7X', 817395179),
('KD3625J', -1195193230, 'ZC4', 1363127590);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;