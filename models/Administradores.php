<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "administradores".
 *
 * @property string $Nombre
 * @property string $Apellidos
 * @property string $Puesto
 * @property int $Tarjeta_Social
 *
 * @property Dirigen[] $dirigens
 * @property Empleados[] $dNIEmpleados
 */
class Administradores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'administradores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Apellidos', 'Puesto', 'Tarjeta_Social'], 'required'],
            [['Tarjeta_Social'], 'integer'],
            [['Nombre', 'Apellidos'], 'string', 'max' => 40],
            [['Puesto'], 'string', 'max' => 100],
            [['Tarjeta_Social'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Nombre' => 'Nombre',
            'Apellidos' => 'Apellidos',
            'Puesto' => 'Puesto',
            'Tarjeta_Social' => 'Tarjeta Social',
        ];
    }

    /**
     * Gets query for [[Dirigens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirigens()
    {
        return $this->hasMany(Dirigen::className(), ['Tarjeta_Social' => 'Tarjeta_Social']);
    }

    /**
     * Gets query for [[DNIEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDNIEmpleados()
    {
        return $this->hasMany(Empleados::className(), ['DNI' => 'DNI_Empleado'])->viaTable('dirigen', ['Tarjeta_Social' => 'Tarjeta_Social']);
    }
}
