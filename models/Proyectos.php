<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proyectos".
 *
 * @property int $Id
 * @property string $Fecha
 * @property int $Revision
 *
 * @property Trabajan[] $trabajans
 * @property Empleados[] $dNIEmpleados
 */
class Proyectos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proyectos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Id', 'Fecha', 'Revision'], 'required'],
            [['Id', 'Revision'], 'integer'],
            [['Fecha'], 'safe'],
            [['Id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Fecha' => 'Fecha',
            'Revision' => 'Revision',
        ];
    }

    /**
     * Gets query for [[Trabajans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajans()
    {
        return $this->hasMany(Trabajan::className(), ['Id' => 'Id']);
    }

    /**
     * Gets query for [[DNIEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDNIEmpleados()
    {
        return $this->hasMany(Empleados::className(), ['DNI' => 'DNI_Empleado'])->viaTable('trabajan', ['Id' => 'Id']);
    }
}
