<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trabajan".
 *
 * @property int $Id
 * @property int $Cod_Grupo
 * @property int $DNI_Empleado
 *
 * @property Proyectos $id
 * @property Grupos $codGrupo
 * @property Empleados $dNIEmpleado
 */
class Trabajan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trabajan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Id', 'Cod_Grupo', 'DNI_Empleado'], 'required'],
            [['Id', 'Cod_Grupo', 'DNI_Empleado'], 'integer'],
            [['Id', 'DNI_Empleado'], 'unique', 'targetAttribute' => ['Id', 'DNI_Empleado']],
            [['Id', 'Cod_Grupo', 'DNI_Empleado'], 'unique', 'targetAttribute' => ['Id', 'Cod_Grupo', 'DNI_Empleado']],
            [['Id'], 'exist', 'skipOnError' => true, 'targetClass' => Proyectos::className(), 'targetAttribute' => ['Id' => 'Id']],
            [['Cod_Grupo'], 'exist', 'skipOnError' => true, 'targetClass' => Grupos::className(), 'targetAttribute' => ['Cod_Grupo' => 'Cod_Grupo']],
            [['DNI_Empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['DNI_Empleado' => 'DNI']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Cod_Grupo' => 'Cod Grupo',
            'DNI_Empleado' => 'Dni Empleado',
        ];
    }

    /**
     * Gets query for [[Id]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId()
    {
        return $this->hasOne(Proyectos::className(), ['Id' => 'Id']);
    }

    /**
     * Gets query for [[CodGrupo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodGrupo()
    {
        return $this->hasOne(Grupos::className(), ['Cod_Grupo' => 'Cod_Grupo']);
    }

    /**
     * Gets query for [[DNIEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDNIEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['DNI' => 'DNI_Empleado']);
    }
}
