<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados".
 *
 * @property string $Nombre
 * @property string $Apellidos
 * @property int $DNI
 *
 * @property Becarios[] $becarios
 * @property Dirigen[] $dirigens
 * @property Administradores[] $tarjetaSocials
 * @property Trabajan[] $trabajans
 * @property Proyectos[] $ids
 */
class Empleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Apellidos', 'DNI'], 'required'],
            [['DNI'], 'integer'],
            [['Nombre', 'Apellidos'], 'string', 'max' => 40],
            [['DNI'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Nombre' => 'Nombre',
            'Apellidos' => 'Apellidos',
            'DNI' => 'Dni',
        ];
    }

    /**
     * Gets query for [[Becarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBecarios()
    {
        return $this->hasMany(Becarios::className(), ['DNI_Tutor' => 'DNI']);
    }

    /**
     * Gets query for [[Dirigens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirigens()
    {
        return $this->hasMany(Dirigen::className(), ['DNI_Empleado' => 'DNI']);
    }

    /**
     * Gets query for [[TarjetaSocials]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTarjetaSocials()
    {
        return $this->hasMany(Administradores::className(), ['Tarjeta_Social' => 'Tarjeta_Social'])->viaTable('dirigen', ['DNI_Empleado' => 'DNI']);
    }

    /**
     * Gets query for [[Trabajans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajans()
    {
        return $this->hasMany(Trabajan::className(), ['DNI_Empleado' => 'DNI']);
    }

    /**
     * Gets query for [[Ids]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIds()
    {
        return $this->hasMany(Proyectos::className(), ['Id' => 'Id'])->viaTable('trabajan', ['DNI_Empleado' => 'DNI']);
    }
}
