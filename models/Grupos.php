<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grupos".
 *
 * @property string $Nombre_Grupo
 * @property int $Integrantes
 * @property int $Cod_Grupo
 *
 * @property Trabajan[] $trabajans
 */
class Grupos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grupos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre_Grupo', 'Integrantes', 'Cod_Grupo'], 'required'],
            [['Integrantes', 'Cod_Grupo'], 'integer'],
            [['Nombre_Grupo'], 'string', 'max' => 40],
            [['Cod_Grupo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Nombre_Grupo' => 'Nombre Grupo',
            'Integrantes' => 'Integrantes',
            'Cod_Grupo' => 'Cod Grupo',
        ];
    }

    /**
     * Gets query for [[Trabajans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajans()
    {
        return $this->hasMany(Trabajan::className(), ['Cod_Grupo' => 'Cod_Grupo']);
    }
}
