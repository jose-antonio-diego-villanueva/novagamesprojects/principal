<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "becarios".
 *
 * @property string $Nombre
 * @property int $Horas_Trabajadas
 * @property string $Tutor
 * @property int $DNI_Tutor
 *
 * @property Empleados $dNITutor
 */
class Becarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'becarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Horas_Trabajadas', 'Tutor', 'DNI_Tutor'], 'required'],
            [['Horas_Trabajadas', 'DNI_Tutor'], 'integer'],
            [['Nombre', 'Tutor'], 'string', 'max' => 40],
            [['Tutor'], 'unique'],
            [['DNI_Tutor'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['DNI_Tutor' => 'DNI']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Nombre' => 'Nombre',
            'Horas_Trabajadas' => 'Horas Trabajadas',
            'Tutor' => 'Tutor',
            'DNI_Tutor' => 'Dni Tutor',
        ];
    }

    /**
     * Gets query for [[DNITutor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDNITutor()
    {
        return $this->hasOne(Empleados::className(), ['DNI' => 'DNI_Tutor']);
    }
}
