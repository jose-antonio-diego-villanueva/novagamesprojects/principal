<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dirigen".
 *
 * @property int $Tarjeta_Social
 * @property int $DNI_Empleado
 *
 * @property Administradores $tarjetaSocial
 * @property Empleados $dNIEmpleado
 */
class Dirigen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dirigen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Tarjeta_Social', 'DNI_Empleado'], 'required'],
            [['Tarjeta_Social', 'DNI_Empleado'], 'integer'],
            [['Tarjeta_Social', 'DNI_Empleado'], 'unique', 'targetAttribute' => ['Tarjeta_Social', 'DNI_Empleado']],
            [['Tarjeta_Social'], 'exist', 'skipOnError' => true, 'targetClass' => Administradores::className(), 'targetAttribute' => ['Tarjeta_Social' => 'Tarjeta_Social']],
            [['DNI_Empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['DNI_Empleado' => 'DNI']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Tarjeta_Social' => 'Tarjeta Social',
            'DNI_Empleado' => 'Dni Empleado',
        ];
    }

    /**
     * Gets query for [[TarjetaSocial]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTarjetaSocial()
    {
        return $this->hasOne(Administradores::className(), ['Tarjeta_Social' => 'Tarjeta_Social']);
    }

    /**
     * Gets query for [[DNIEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDNIEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['DNI' => 'DNI_Empleado']);
    }
}
