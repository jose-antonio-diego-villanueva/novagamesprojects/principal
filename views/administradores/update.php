<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Administradores */

$this->title = 'Update Administradores: ' . $model->Tarjeta_Social;
$this->params['breadcrumbs'][] = ['label' => 'Administradores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tarjeta_Social, 'url' => ['view', 'id' => $model->Tarjeta_Social]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="administradores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
