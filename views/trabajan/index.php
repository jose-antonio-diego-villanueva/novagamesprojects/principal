<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trabajos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Trabajan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Id',
            'Cod_Grupo',
            'DNI_Empleado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
