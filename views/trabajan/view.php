<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Trabajan */

$this->title = $model->Id;
$this->params['breadcrumbs'][] = ['label' => 'Trabajans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="trabajan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Id' => $model->Id, 'Cod_Grupo' => $model->Cod_Grupo, 'DNI_Empleado' => $model->DNI_Empleado], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Id' => $model->Id, 'Cod_Grupo' => $model->Cod_Grupo, 'DNI_Empleado' => $model->DNI_Empleado], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Id',
            'Cod_Grupo',
            'DNI_Empleado',
        ],
    ]) ?>

</div>
