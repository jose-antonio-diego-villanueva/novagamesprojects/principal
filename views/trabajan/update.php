<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Trabajan */

$this->title = 'Update Trabajan: ' . $model->Id;
$this->params['breadcrumbs'][] = ['label' => 'Trabajans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Id, 'url' => ['view', 'Id' => $model->Id, 'Cod_Grupo' => $model->Cod_Grupo, 'DNI_Empleado' => $model->DNI_Empleado]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trabajan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
