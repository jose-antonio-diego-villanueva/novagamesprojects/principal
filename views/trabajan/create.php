<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Trabajan */

$this->title = 'Create Trabajan';
$this->params['breadcrumbs'][] = ['label' => 'Trabajans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
