<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Dirigen */

$this->title = $model->Tarjeta_Social;
$this->params['breadcrumbs'][] = ['label' => 'Dirigens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="dirigen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Tarjeta_Social' => $model->Tarjeta_Social, 'DNI_Empleado' => $model->DNI_Empleado], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Tarjeta_Social' => $model->Tarjeta_Social, 'DNI_Empleado' => $model->DNI_Empleado], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Tarjeta_Social',
            'DNI_Empleado',
        ],
    ]) ?>

</div>
