<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dirigen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dirigen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Tarjeta_Social')->textInput() ?>

    <?= $form->field($model, 'DNI_Empleado')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
