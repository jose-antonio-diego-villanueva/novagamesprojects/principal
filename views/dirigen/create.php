<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dirigen */

$this->title = 'Create Dirigen';
$this->params['breadcrumbs'][] = ['label' => 'Dirigens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dirigen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
