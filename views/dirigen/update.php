<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dirigen */

$this->title = 'Update Dirigen: ' . $model->Tarjeta_Social;
$this->params['breadcrumbs'][] = ['label' => 'Dirigens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tarjeta_Social, 'url' => ['view', 'Tarjeta_Social' => $model->Tarjeta_Social, 'DNI_Empleado' => $model->DNI_Empleado]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dirigen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
