<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dirección';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dirigen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Dirigen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Tarjeta_Social',
            'DNI_Empleado',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
