<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Becarios */

$this->title = 'Update Becarios: ' . $model->Tutor;
$this->params['breadcrumbs'][] = ['label' => 'Becarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Tutor, 'url' => ['view', 'id' => $model->Tutor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="becarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
