<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Becarios */

$this->title = 'Create Becarios';
$this->params['breadcrumbs'][] = ['label' => 'Becarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="becarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
