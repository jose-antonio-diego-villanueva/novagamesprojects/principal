<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Becarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="becarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Horas_Trabajadas')->textInput() ?>

    <?= $form->field($model, 'Tutor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DNI_Tutor')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
