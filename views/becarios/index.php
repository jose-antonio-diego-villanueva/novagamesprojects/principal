<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Becarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="becarios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Becarios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Nombre',
            'Horas_Trabajadas',
            'Tutor',
            'DNI_Tutor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
